#include "beamform.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char *argv[]){
    FILE *ifl;
    beamform beam;
    int ierr;

    if (argc != 2){
       ierr = usage();
       exit(ierr);
    }

    ifl = fopen(argv[1],"rb");
    fread(&beam,sizeof(beam),1,ifl);
    fprintf(stdout,"delta: %5.3f\n",beam.delta);
    fprintf(stdout,"ndata: %d\n",beam.ndata);
    fprintf(stdout,"array_centroid_lat: %5.3f\n",beam.a_lat);
    fprintf(stdout,"array_centroid_lon: %5.3f\n",beam.a_lon);
    fprintf(stdout,"event_lat: %5.3f\n",beam.e_lat);
    fprintf(stdout,"event_lon: %5.3f\n",beam.e_lon);
    fprintf(stdout,"event_depth: %5.3f\n",beam.e_dep);
    fprintf(stdout,"baz_min: %d\n",beam.baz_min);
    fprintf(stdout,"baz_max: %d\n",beam.baz_max);
    fprintf(stdout,"baz_inc: %d\n",beam.baz_inc);
    fprintf(stdout,"i_min: %d\n",beam.i_min);
    fprintf(stdout,"i_max: %d\n",beam.i_max);
    fprintf(stdout,"i_inc: %d\n",beam.i_inc);
}

int usage(){
    fprintf(stdout,"USAGE: xh_beamdescribe BEAMFILE\n");
    fprintf(stdout,"BEAMFILE is output binary file from xh_beamform\n");
    fprintf(stdout,"\n");
    return(0);
}





