#!/bin/bash

gcc ascii_rotate_ne_rt.c -lm -o ../bin/ascii_rotate_ne_rt
gcc ascii_rotate_rt_ne.c -lm -o ../bin/ascii_rotate_rt_ne
gcc ascii_rotate_nez_lqt.c -lm -o ../bin/ascii_rotate_nez_lqt
gcc vincenty.c az_gcarc.c -lm -o ../bin/az_gcarc
gcc vincenty.c az_spread.c -lm -o ../bin/az_spread
gcc vincenty.c gc_spread.c -lm -o ../bin/gc_spread
gcc vincenty.c vincenty_direct.c -lm -o ../bin/vincenty_direct
gcc vincenty.c vincenty_inverse.c -lm -o ../bin/vincenty_inverse
gcc vincenty.c vincenty_direct_file.c -lm -o ../bin/vincenty_direct_file
gcc vincenty.c vincenty_inverse_file.c -lm -o ../bin/vincenty_inverse_file
gcc source_stat_midpoint.c $LIBDIR/libdist.a -lm -o ../bin/source_stat_midpoint
gcc bin_midpoint.c $LIBDIR/libdist.a -lm -o ../bin/bin_midpoint
gcc strike_dip_rake_to_CMTSOLUTION.c -lm -o ../bin/strike_dip_rake_to_CMTSOLUTION

